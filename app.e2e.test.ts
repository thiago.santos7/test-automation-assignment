import request from "supertest";
import faker from "faker";

import app from "./app";

jest.useFakeTimers();

describe("Operation endpoint", () => {
  let requestData = {
    username: faker.name.firstName(),
    op: faker.random.arrayElement(["add", "sub", "mul", "div"]),
    values: {
      a: faker.datatype.number({ max: 50, min: 1 }),
      b: faker.datatype.number({ max: 50, min: 1 }),
    },
    results: undefined,
  };

  test("should make operations correctly", async () => {
    const res = await request(app)
      .post("/make-operation")
      .send(requestData)
      .expect(200);

    expect(res.body).toHaveProperty("result");
    requestData.results = res.body.result;
  });

  test("should return user history with correct values", async () => {
    const res = await request(app)
      .get("/user-history")
      .send({
        username: requestData.username,
      })
      .expect(200);

    expect(res.body).toEqual({
      userOps: [
        {
          op: requestData.op,
          result: requestData.results,
          values: [requestData.values.a, requestData.values.b],
        },
      ],
    });
  });

  test("should return error if value is invalid", async () => {
    const res = await request(app)
      .post("/make-operation")
      .send({
        ...requestData,
        op: "div",
        values: { ...requestData.values, b: 0 },
      })
      .expect(400);

    expect(res.body).toHaveProperty("error", "Cannot divide by zero!");
  });
});

describe("User history endpoint", () => {
  let requestData = {
    username: faker.name.firstName(),
  };

  test("should return user history", async () => {
    const res = await request(app)
      .get("/user-history")
      .send(requestData)
      .expect(200);

    expect(res.body).toEqual({
      userOps: [],
    });
  });
});
