import express from "express";
import logger from "./logger";
import {
  getUser,
  handleUserOperation,
  validateOperationRequest,
} from "./utils";

const app = express();

const users = new Map();

app.use(express.json());

app.use((req, res, next) => {
  if (req.url === "/make-operation") {
    const validationResult = validateOperationRequest(req.body);

    if (typeof validationResult === "string") {
      logger.error(`${req.headers.host} - ${validationResult}`);

      return res.status(400).json({ error: validationResult });
    }

    next();
  }

  if (req.url === "/user-history") {
    if (!req.body.username) {
      logger.error(
        `${req.headers.host} - Invalid request! You need to provide a username to get a user history.`
      );

      return res.status(400).json({
        error:
          "Invalid request! You need to provide a username to get a user history.",
      });
    }

    next();
  }
});

app.get("/user-history", (req, res) => {
  const { username, op } = req.body;

  logger.info(
    `${username} has requested the history of their ${op ?? ""} operations.`
  );

  const userOps = getUser(username, users, op);

  return res.status(200).json({ userOps });
});

app.post("/make-operation", (req, res) => {
  const { username, op, values } = req.body;

  logger.info(
    `${username} has requested ${op} operation with values ${values.a} and ${values.b}.`
  );

  try {
    const result = handleUserOperation(username, op, values, users);
    return res.status(200).json({ result });
  } catch (err) {
    logger.error(
      `User: ${username}, OP: ${op} and values: ${JSON.stringify(values)} - ${
        err.message
      }`
    );
    return res.status(400).json({ error: err.message });
  }
});

export default app;
