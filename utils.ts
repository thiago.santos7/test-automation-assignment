function add(x: number, y: number) {
  return x + y;
}

function subtract(x: number, y: number) {
  return x - y;
}

function multiply(x: number, y: number) {
  return x * y;
}

function divide(x: number, y: number) {
  if (y === 0) {
    throw new Error("Cannot divide by zero!");
  }

  return x / y;
}

function getUser(username: string, users: Users, reqOp?: string) {
  if (users.has(username)) {
    if (reqOp) {
      return users.get(username)!.filter(({ op }) => op === reqOp);
    }
    return users.get(username);
  }
  return users.set(username, []).get(username);
}

function makeOperation(op: string, a: number, b: number) {
  switch (op) {
    case "add":
      return add(a, b);
    case "sub":
      return subtract(a, b);
    case "mul":
      return multiply(a, b);
    case "div":
      return divide(a, b);
    default:
      return a ?? b;
  }
}

type Users = Map<
  string,
  Array<{ op: string; values: Array<number>; result: number }>
>;

type Values = {
  a?: number;
  b?: number;
};
function handleUserOperation(
  username: string,
  op: string,
  { a, b }: Values,
  users: Users
) {
  const user = getUser(username, users);

  const result = makeOperation(op, a!, b!);

  user?.push({
    op,
    values: [a!, b!],
    result,
  });

  return result;
}

function validateOperationRequest(body: {
  username?: string;
  op?: string;
  values?: Values;
}) {
  if (!body.username) {
    return "Invalid request! You need to provide a user username to make operations.";
  }
  if (!["add", "sub", "mul", "div"].includes(body.op ?? "")) {
    return 'Invalid operation! You can make one of the following operations: "add", "sub", "mul" and "div".';
  }
  if (
    typeof body.values?.a !== "number" ||
    typeof body.values?.b !== "number"
  ) {
    return "Invalid operation! You can only make operations on valid numbers.";
  }

  return body;
}

export {
  add,
  subtract,
  multiply,
  divide,
  getUser,
  makeOperation,
  handleUserOperation,
  validateOperationRequest,
};
