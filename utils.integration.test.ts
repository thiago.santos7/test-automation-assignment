import faker from "faker";
import { makeOperation, handleUserOperation } from "./utils";

const makeSUT = () => ({
  name: faker.name.firstName(),
  a: faker.datatype.number({ max: 50, min: 1 }),
  b: faker.datatype.number({ max: 50, min: 1 }),
});

describe("Operation", () => {
  const users = new Map();

  beforeAll(() => {
    users.clear();
  });

  describe("makeOperation", () => {
    test("should return correct operation result", () => {
      const { a, b } = makeSUT();

      expect(makeOperation("add", a, b)).toBe(a + b);
      expect(makeOperation("sub", a, b)).toBe(a - b);
      expect(makeOperation("mul", a, b)).toBe(a * b);
      expect(makeOperation("div", a, b)).toBe(a / b);
    });

    test("should throw invalid value error", () => {
      const { a } = makeSUT();

      expect(() => makeOperation("div", a, 0)).toThrow(
        "Cannot divide by zero!"
      );
    });
  });

  describe("handleUserOperation", () => {
    test("should handle user operations correctly", () => {
      const { name, a, b } = makeSUT();

      expect(handleUserOperation(name, "add", { a, b }, users)).toBe(a + b);
      expect(handleUserOperation(name, "sub", { a, b }, users)).toBe(a - b);
      expect(handleUserOperation(name, "mul", { a, b }, users)).toBe(a * b);
      expect(handleUserOperation(name, "div", { a, b }, users)).toBe(a / b);

      const userOps = users.get(name);

      expect(userOps).toHaveLength(4);
      expect(userOps).toEqual([
        { op: "add", values: [a, b], result: a + b },
        { op: "sub", values: [a, b], result: a - b },
        { op: "mul", values: [a, b], result: a * b },
        { op: "div", values: [a, b], result: a / b },
      ]);
    });

    test("should throw invalid value error", () => {
      const { name, a } = makeSUT();

      expect(() =>
        handleUserOperation(name, "div", { a, b: 0 }, users)
      ).toThrow("Cannot divide by zero!");
    });
  });
});
