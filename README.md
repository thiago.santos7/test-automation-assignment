# Test Automation Assignment

This project is an app to make simple math operations with HTTP requests.

## Tools

- Typescript;
- Elastic APM;
- Express;
- Winston;
- Babel;
- Jest;
- Faker;
- Supertest.

## How to execute

Install dependencies with:

```
npm install
```

And just run normally:

```
npm start
```

To run the tests:

```
npm test
```

## Routes

### Make operation

```
http://localhost:3000/make-operation
```

#### Expected input and output

##### Input

```json
{
  "username": "string",
  "op": "add|sub|mul|div",
  "values": {
    "a": "number",
    "b": "number"
  }
}
```

##### Output

```json
{
  "result": "number"
}
```

```
http://localhost:3000/user-history
```

### Expected input and output

#### Get user history

##### Input

```json
{
  "username": "string"
}
```

##### Output

```json
{
  "userOps": [
    {
      "op": "string",
      "values": {
        "a": "number",
        "b": "number"
      },
      "result": "number"
    }
  ]
}
```
