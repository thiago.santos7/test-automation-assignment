import {
  add,
  getUser,
  divide,
  multiply,
  subtract,
  validateOperationRequest,
} from "./utils";
import faker from "faker";

const makeSUT = () => ({
  name: faker.name.firstName(),
  a: faker.datatype.number({ max: 50, min: 1 }),
  b: faker.datatype.number({ max: 50, min: 1 }),
});

describe("Math", () => {
  describe("add", () => {
    test("should add two values", () => {
      const { a, b } = makeSUT();

      expect(add(a, b)).toBe(a + b);
    });
  });

  describe("subtract", () => {
    test("should subtract two values", () => {
      const { a, b } = makeSUT();

      expect(subtract(a, b)).toBe(a - b);
    });
  });

  describe("multiply", () => {
    test("should multiply two values", () => {
      const { a, b } = makeSUT();

      expect(multiply(a, b)).toBe(a * b);
    });
  });

  describe("divide", () => {
    test("should divide two values", () => {
      const { a, b } = makeSUT();

      expect(divide(a, b)).toBe(a / b);
    });

    test("should throw invalid value error", () => {
      const { a } = makeSUT();

      expect(() => divide(a, 0)).toThrow("Cannot divide by zero!");
    });
  });
});

describe("User", () => {
  describe("createUser", () => {
    const users = new Map();
    const { name } = makeSUT();

    beforeAll(() => {
      users.clear();
    });

    test("should add a new user", () => {
      expect(getUser(name, users)).toHaveLength(0);
      expect(Object.fromEntries(users)).toEqual({ [name]: [] });
    });

    test("should return an old user if it was already added", () => {
      getUser(name, users);

      expect(getUser(name, users)).toHaveLength(0);
      expect(Object.fromEntries(users)).toEqual({ [name]: [] });
    });

    test("should return only the request user operation history", () => {
      users.set(name, [
        {
          op: "add",
          values: [3, 5],
          result: 8,
        },
        {
          op: "mul",
          values: [3, 5],
          result: 15,
        },
      ]);

      expect(getUser(name, users, "div")).toHaveLength(0);
      expect(getUser(name, users, "add")).toEqual([
        {
          op: "add",
          values: [3, 5],
          result: 8,
        },
      ]);
    });
  });
});

describe("Validation", () => {
  test("should return body if request body is valid", () => {
    const body = {
      username: "Janaina",
      op: "add",
      values: {
        a: 2,
        b: 6,
      },
    };

    expect(validateOperationRequest(body)).toBe(body);
  });

  test("should return error message if request body is invalid", () => {
    const bodyWithoutUsername = {
      op: "add",
      values: {
        a: 2,
        b: 6,
      },
    };
    const bodyWithoutOperation = {
      username: "Janaina",
      values: {
        a: 2,
        b: 6,
      },
    };
    const bodyWithoutValue = {
      username: "Janaina",
      op: "add",
      values: {
        a: 2,
      },
    };

    expect(validateOperationRequest(bodyWithoutUsername)).toBe(
      "Invalid request! You need to provide a user username to make operations."
    );
    expect(validateOperationRequest(bodyWithoutOperation)).toBe(
      'Invalid operation! You can make one of the following operations: "add", "sub", "mul" and "div".'
    );
    expect(validateOperationRequest(bodyWithoutValue)).toBe(
      "Invalid operation! You can only make operations on valid numbers."
    );
  });
});
